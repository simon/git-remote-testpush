git-remote-testpush is a git helper program to allow dry-run pushes
without using the remote's push API.

Scenario: you've set up a git remote with different URLs for pull and
push, and the push URL needs some kind of awkward authentication (e.g.
an ssh key stored on a hardware device that demands a PIN every time).

You're about to do a git push to this remote, but first, you want to
check that you've got your command line right, and it really is going
to push the branch(es) you wanted, and not some other branch.

'git push -n' will talk to the remote using the push URL. This is
great if you want to check that the push URL is working. But if you're
only trying to check that your command isn't nonsense, it means you
have to go through the push URL authentication an extra time.

For this scenario, you'd like a different kind of dry-run push, which
contacts the repository using the easier-to-use _pull_ URL, and lets
you do dry-run pushes but (obviously) not real ones. This checks less
than the ordinary 'push -n', but it still checks the thing you most
wanted, and costs you less effort.

This remote helper provides just that. You can invoke it by prefixing
'testpush::' to your existing remote name or URL, e.g.

  git push -n testpush::origin [refspecs]
  git push -n testpush::https://example.com/example.git [refspecs]

To install:
 - have a Python 3 interpreter available
 - put this script somewhere on your PATH.
