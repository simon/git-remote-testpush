#!/usr/bin/env python3

'''A git helper to allow dry-run pushes without using the remote's push API.

Scenario: you've set up a git remote with different URLs for pull and
push, and the push URL needs some kind of awkward authentication (e.g.
an ssh key stored on a hardware device that demands a PIN every time).

You're about to do a git push to this remote, but first, you want to
check that you've got your command line right, and it really is going
to push the branch(es) you wanted, and not some other branch.

'git push -n' will talk to the remote using the push URL. This is
great if you want to check that the push URL is working. But if you're
only trying to check that your command isn't nonsense, it means you
have to go through the push URL authentication an extra time.

For this scenario, you'd like a different kind of dry-run push, which
contacts the repository using the easier-to-use _pull_ URL, and lets
you do dry-run pushes but (obviously) not real ones. This checks less
than the ordinary 'push -n', but it still checks the thing you most
wanted, and costs you less effort.

This remote helper provides just that. You can invoke it by prefixing
'testpush::' to your existing remote name or URL, e.g.

  git push -n testpush::origin [refspecs]
  git push -n testpush::https://example.com/example.git [refspecs]

'''

import argparse
import os
import shlex
import subprocess
import sys

debugfh = None
our_command_name = "git-remote-testpush"
our_remote_name = "testpush"

def fatal(text):
    sys.exit(f"{our_command_name}: fatal: {text}")

def git(*args, allowed_statuses=range(0, 1)):
    cmd = ["git"] + list(args)
    if debugfh is not None:
        print("run:", shlex.join(cmd), file=debugfh)
    with open(os.devnull) as devnullfh:
        p = subprocess.Popen(cmd, stdin=devnullfh,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        exitstatus = p.wait()
    if debugfh is not None:
        print(f"exit status = {exitstatus:#x}", file=debugfh)
        print(f"stdout = {out!r}", file=debugfh)
        print(f"stderr = {err!r}", file=debugfh)
    if exitstatus not in allowed_statuses:
        fatal("\n".join(
            [f"command {shlex.join(cmd)} failed with status {exitstatus:#x}",
             err.decode('utf-8', errors='replace').rstrip("\n")]))

    out = out.decode('utf-8', errors='replace')
    if len(allowed_statuses) > 1:
        return out, exitstatus
    else:
        return out

def remote_helper_protocol(infh, outfh, remote):
    dry_run = False

    while True:
        cmd = infh.readline().decode('utf-8', errors='replace')
        if cmd == "":
            if debugfh is not None:
                print("got EOF", file=debugfh)
            break
        cmd = cmd.rstrip('\n')

        if debugfh is not None:
            print(f"got command '{cmd}'", file=debugfh)

        if cmd == 'capabilities':
            outfh.write(b'option\npush\n\n')

        elif cmd == 'option dry-run true':
            dry_run = True
            outfh.write(b'ok\n')

        elif cmd.startswith('option '):
            outfh.write(b'unsupported\n')

        elif cmd == 'list for-push':
            output = git('ls-remote', remote)
            for line in output.splitlines():
                objhash, ref = line.split('\t', 1)
                outfh.write(f"{objhash} {ref}\n".encode('utf-8'))
            outfh.write(b'\n')

        elif cmd.startswith('push '):
            spec = cmd[5:]

            force = False
            if spec.startswith('+'):
                spec = spec[1:]
                force = True

            srcspec, dstspec = spec.split(':')

            srchash = git('rev-parse', srcspec).rstrip('\n')

            if not dry_run:
                outfh.write(
                    f"error {dstspec} {our_remote_name} doesn't accept "
                    f"real pushes!\n".encode('utf-8'))
            elif force:
                outfh.write(f"ok {dstspec}\n".encode('utf-8'))
            else:
                # The local git client will detect non-fast-forward
                # pushes without -f, based on the output from 'list
                # for-push', if it has the objects available in the
                # local object store to test ancestry. If it doesn't,
                # we must fetch the current state of the remote ref
                # ourselves, and do the test here (mimicking the way
                # that a real remote would do the test on the server).

                tmpref = 'refs/testpush-tmp'
                _, st = git('fetch', '--no-write-fetch-head', remote,
                            f"+{dstspec}:{tmpref}", allowed_statuses=range(256))
                if st != 0:
                    # If this failed, assume it's because that ref
                    # doesn't exist yet, so that the push would create it
                    outfh.write(f"ok {dstspec}\n".encode('utf-8'))
                else:
                    try:
                        dstorig = git('rev-parse', tmpref).rstrip('\n')
                    finally:
                        # Delete our temporary ref, whether that worked or not
                        try:
                            git('update-ref', '-d', tmpref)
                        except subprocess.CalledProcessError:
                            pass
                    _, st = git('merge-base', '--is-ancestor', dstorig, srchash,
                                allowed_statuses=range(0,2))
                    if st == 0:
                        outfh.write(f"ok {dstspec}\n".encode('utf-8'))
                    else:
                        outfh.write(f"error {dstspec} non-fast-forward\n"
                                    .encode('utf-8'))

        elif cmd == '':
            # An empty command is sent at the end of a batch of
            # pushes. Respond with an empty output.
            outfh.write(b'\n')

        else:
            fatal("command '{cmd}' not supported")

        outfh.flush()

    outfh.flush()

def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("remote", help="Original name of git remote, as provided on the git command line (e.g. may be a name like 'origin')")
    parser.add_argument("url", nargs="?", help="Full URL of git remote, after dereferencing configured names")
    args = parser.parse_args()

    if args.url is None:
        args.url = args.remote

    if os.environ.get("GIT_REMOTE_TESTPUSH_DEBUG", "").lower() in {
            "1", "y", "true", "on"}:
        global debugfh
        debugfh = sys.stderr

    # We allow users to install this as a differently named remote
    # helper if they like. Therefore, we figure out what our own
    # remote name is by examining argv[0].
    global our_command_name
    our_command_name = os.path.basename(sys.argv[0])
    command_prefix = "git-remote-"
    assert our_command_name.startswith(command_prefix), (
        f"invoked as '{our_command_name}: cannot determine remote type")
    global our_remote_name
    our_remote_name = our_command_name[len(command_prefix):]

    # We allow users to invoke us as testpush::[remote] or
    # testpush://[remote] as they choose, which means we may or may
    # not have to trim our own remote type off the front of the URL.
    url_prefix = our_remote_name + "://"
    if args.url.startswith(url_prefix):
        args.url = args.url[len(url_prefix):]

    # Now we know what our underlying remote name is, and we're ready
    # to speak the git remote-helper protocol.
    try:
        remote_helper_protocol(sys.stdin.buffer, sys.stdout.buffer, args.url)
    except BrokenPipeError:
        os._exit(0) # exit quietly if this happens

if __name__ == '__main__':
    main()
